from flask import Flask, render_template, request
from scrape import createUrl, getItems
import re

app = Flask(__name__)

@app.route("/")
def home():
	return render_template("index.html")

#main function that takes in the form and renders the links to the jobs
@app.route("/submit", methods = ["POST"])
def submit():
	email = request.form["email"]
	jobTitle = request.form["jobTitle"]
	wage_floor = request.form["gridRadios"]
	location = request.form["location"]
	location = re.findall(r"[\w']+", location)
	words = jobTitle.split()
	url = createUrl(email, words, wage_floor, location)
	items = getItems(url)
	return render_template("results.html", items=items)

if __name__ == "__main__":
	app.run(debug=True, host='0.0.0.0')
import requests
import urllib3
import re
import bs4
from bs4 import BeautifulSoup
from urllib.request import urlopen  
import pandas as pd
import time

#function creates URL from input parameters from user
def createUrl(email, jobTitle, desiredWage, location):
    startUrl = "https://www.indeed.com/jobs?q="
    num_job_words = len(jobTitle)
    seperator = "+"
    jobPiece = seperator.join(jobTitle)
    location = "-".join(location)
    startUrl = startUrl + jobPiece + "+$" + desiredWage + "&l=" + location
    return startUrl

#get items from webscraper
def getItems(URL):
	page = requests.get(URL)
	soup = BeautifulSoup(page.text, "html.parser")

	results = soup.find_all('div', attrs={'class': 'jobsearch-SerpJobCard'})
	resultItems = []
	for element in results:
		items = []
		title = None
		summary = None
		company = None
		location = None
		salary = None
		link = None
		title = element.find('div', attrs={'class':'title'}).text.strip()
		titleAttribute = element.find('a', href = True)
		link = "https://www.indeed.com" + titleAttribute['href']
		newPage = requests.get(link)
		soup1 = BeautifulSoup(newPage.text, "html.parser")
		summary = soup1.find('div', attrs={'class': 'jobsearch-jobDescriptionText'}).text.strip()
		company = element.find('span', attrs={'class':'company'}).text.strip()
		# summary = element.find('div', attrs={'class':'summary'}).text.strip()
		# summary = re.sub('[.,]', '', summary)
		location = element.find('div', attrs={'class':'location'})
		salary = element.find('span', attrs={'class':'salary'})
		if(salary != None):
			salary = salary.text.strip()
			print ('salary: {} \n'.format(salary))
		else:
			salary = "NA"
		if(location != None):
			location = location.text.strip()
			print ('location: {} \n'.format(location))
		else:
			location = "NA"
		items.append(title)
		items.append(summary)
		items.append(company)
		items.append(location)
		items.append(salary)
		items.append(link)
		resultItems.append(items)
	return resultItems